---
title: Me
description: Everything you didn't want to know about me
published: true
date: 2020-08-15T17:41:55.597Z
tags: me, personal, about
editor: markdown
---

# Person
:wave: Mine name is Elias, I lived a good part of my life in Campinas, São Paulo - Brasil. Currently I'm living in a very different country in Europe.

I work as a software engineer, at the moment for a fin-tech. Previously I worked for a big cloud corporation. My current plan is to somehow be able to pay my bills working only with free software.

I'm vegan and I really enjoy talking and reading about society. I like to think myself as an anarchist, because I reject all involuntary, coercive forms of hierarchy. That includes: the State and the Capital.

I do enjoy to write, though you can argue that I'm not good at it ;). When I'm writing about workplace and work in general, I try to do at [masterless.io](https://www.masterless.io/). I log my life (things I cook, random thoughts) in this wiki. I'm trying to write in Portuguese again at [brtech.ie](https://www.brtech.ie), mainly about tips to Brasilians that want to work with tech in Ireland. But it's a bit hard for me, since I mainly distrust / not in the mood of big tech / corporations.

## Now

- Thinking why the Global North is the source of tech news, if the Global South has an amazing and sometimes stronger free software communities
- [Reading Q](https://en.wikipedia.org/wiki/Q_%28novel%29)
- [Reading Operating Systems - Three easy pieces](http://pages.cs.wisc.edu/~remzi/OSTEP/)
- Taking care of an adopted cat named Madoka :cat:<3
- Trying to buy a house
- [Trying to free myself from social networks](https://wiki.anarchist.work/tech/no-social-networks), deleted Facebook and Instagram; trying to exit twitter.
- Probably burnout due to several extra hours of work per week 

![](https://static.fsf.org/nosvn/associate/crm/642423.png)