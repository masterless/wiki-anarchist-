---
title: Manifestos
description: A list of interesting manifestos
published: true
date: 2020-08-12T22:47:30.286Z
tags: 
editor: undefined
---

# Tech-political manifestos

- [A hacker manifesto](https://en.wikipedia.org/wiki/A_Hacker_Manifesto)
- [Anarchohacker manifesto](https://theanarchistlibrary.org/library/anarchohacker-manifesto-2018) found thanks to [Hispagatos](https://hispagatos.org/)
- [The dotCommunism manifesto](http://emoglen.law.columbia.edu/publications/dcm.html) 
- [Hackmeeting manifesto](https://es.hackmeeting.org/hm/index.php?title=Manifiesto2019eng)
- [Telekommunisten manifesto](http://media.telekommunisten.net/manifesto.pdf) 