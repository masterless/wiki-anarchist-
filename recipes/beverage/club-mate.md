---
title: Club-Mate
description: A la brasileira
published: true
date: 2020-08-09T20:30:48.742Z
tags: 
editor: undefined
---

# Club-Mate

Club-mate is a German soda which main ingredient is Yerba Mate. For a reason that I don't know the drink is kinda famous in the hacker and club scene. 

The drink itself looks a bit like a [carbonated terere](https://en.wikipedia.org/wiki/Terer%C3%A9). After finding the ]Lag( [Mate page](https://ikiwiki.laglab.org/Mate/) I decided to  do something similar, but trying to be closer to [Chimarrão](https://en.wikipedia.org/wiki/Mate_(beverage)) than the original Club-Mate flavour.

I still did not play with ways of making it carbonated, I drunk it as a weird/sweeter terere. I will update once I have done it.

Since I'm in Europe, it means that Yerba Mate is more expensive than in Brasil and the (good) Brasilian herb is more hard to find in Ireland, I decided to use `Cruz de Malta Yerba Mate` as the herb for this drink. The taste is a bit more strong than the Brasilian one.

Before starting, some tips:
- Mate is not green tea! Do not boile the water!! You are going to burn the herb if you do that. Around 80c is more fine to do the tea.

## Test 2
- ~1L of water
- 10 tablespoon of Mate (this time `Campesino Energy with Katuava and Ginseng`)
- 3 tablespoon of Maple Syrup
- 1/8 teaspoon of Citric Acid
- 1/2 teaspoon of molasses
- a bit of black pepper
- a bit of vanilla
- a bit of cinnamon
- a bit of salt

### Steps

1. Make a liter of mate tea (never boiling the water). And leave it for 10 minutes to "rest"
1. Mix all the ingredients.
1. Drink it cold ;)

## Test 1
![First liter of 'Club-Mate'](/beverages/img_20200803_132602.jpg =250x)

### Ingredients

- ~1L of water
- 5 tablespoon of Mate
- 2 tablespoon of Mapple Syrup
- 1 Tablespoon of Agave Syrup
- 1 teaspoon of Catuaba powder (for energy :) )
- 1 teaspoon of Guaraná powder (for energy^2)
- 1/8 teaspoon of Citric Acid
- 1/2 teaspoon of molasses

### Steps

1. Make a liter of mate tea (never boiling the water). And leave it for 10 minutes to "rest"
1. Mix all the ingredients.
1. Drink it cold ;)