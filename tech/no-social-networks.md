---
title: Why I stopped using social networks
description: Privacy was not my main concern :)
published: true
date: 2020-08-15T17:36:45.401Z
tags: me, personal, about
editor: markdown
---

# Quick reasons

- You are forced to login to access most of the content and in exchange they spy on you.
- If one day the social network is no more you probably will lose all your content (maybe they allow you to download it in weird formats that you have to learn how to export to another platform).
- Sense of "if I don't login and check my timeline I will lose that content forever".
- Echo chambers
- Almost zero interested subjects
- Most companies belong to a single country and they have being censoring speeches
- Toxic enviromments 
- Have to thing in a certain format (140 chars, text + image or whatever)
- Creating content that rich people will use to sell shit to friends / followers
- Most social networks are powered by proprietary software

# Are you in any social network?
- I'm on github, Linkedin and Twitter.
  - I cannot at the moment afford to delete my github or Linkedin (happy Microsoft?). I have plans to host my own git service with [gitea](https://gitea.io). Linkedin is harder, I basically need it to find jobs.
  - I'm planning to delete my twitter account, I'm there since 2008 and I made very good friends using it. So it's a bit hard for me. At the moment my account mainly tweets links I fav at pocket or other similar services.