---
title: Anarchist Work
description: Hacking the world
published: true
date: 2020-08-15T17:39:43.708Z
tags: about
editor: markdown
---

# About 

This wiki is where I log my work or discoveries. Although most of it will probably be tech related, you are going to find here vegan receipes, life hacks and random thoughts.

I write about workplace and random political tech stuff at [masterless.io](https://www.masterless.io/). I also write in [Portuguese about working in Ireland as a Software Engineer](https://www.brtech.ie/).

[I do have other social networks](https://wiki.anarchist.work/tech/no-social-networks), but if you want to contact me, just drop me an email at the@communist.dev.

If you want to correct anything written here or add something, let me know.

There's no RSS here, so I'll basically from time to time change this home page with links to new pages. In general, this will be a draft of thoughts that will be transformed in a way or another in posts at [masterless.io](https://www.masterless.io) so I recomend following that [feed](https://www.masterless.io/posts/index.xml).

The wiki content is also on [github](https://github.com/era/wiki.anarchist-). If you want to fix something or add any info, you can by opening a pull request.

I really enjoy discoverying cool blogs / websites, but this days it seems like all contents are locked inside social networks. If you know cool website, feel free to sugest it at [this page](https://wiki.anarchist.work/tech/cool-links). Want to build something similar as we had a few years ago wher people would link the blogs / sites they liked.

Are you curious about me?
- [Check "me" page](https://wiki.anarchist.work/personal/me)

----
## Currently
- Trying to find the perfect formula for [homemade club-mate](https://wiki.anarchist.work/recipes/beverage/club-mate)
- Rescuing a cat named Madoka :3
- [Trying to learn about OS and Rust by following up the blog_os tutorial](https://github.com/era/blog_os)